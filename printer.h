#ifndef PRINTER_H_LQHDRVBY
#define PRINTER_H_LQHDRVBY

#include "observer.h"
#include "board.h"
#include <memory>

template <unsigned S>
class printer : public observer, public std::enable_shared_from_this< observer > {
	constexpr static unsigned _size = S*S;

	void _hline() {
		for (unsigned i = 0; i < ( _size*3 + 2 ); i++) std::cout << "=";
		std::cout << std::endl;
	}

public:
	printer(const std::shared_ptr< board<S> >& b) :
		observer(b)
	{
	}
	virtual ~printer() = default;

	void init() {
		this->_observee->add(this->shared_from_this());
	}

	virtual void update() {
		print();
	}

	void print() {
		auto b = std::dynamic_pointer_cast< board<S> >(_observee);
		for (unsigned y = 0; y < _size; y++) {
			if (!(y%S)) _hline();
			for (unsigned x = 0; x < _size; x++) {
				int value = b->get(x,y);
				std::cout << ( x%S ? " " : "| " );

				if (value)
					std::cout << value;
				else
					std::cout << ' ';
				std::cout << " ";
			} // for
			std::cout << std::endl;
		} // for
		_hline();
	}
};


#endif /* end of include guard: PRINTER_H_LQHDRVBY */

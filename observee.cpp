#include "observee.h"

#include <algorithm>

void observee::update() const {
	// notify all the registered observers
	for (auto i : _o) i->update();
}


void observee::add(const std::shared_ptr<observer>& o) {
	_o.push_back(o);
}

void observee::del(const std::shared_ptr<observer>& o) {
	auto it = std::find(_o.begin(), _o.end(), o);
	if (it != _o.end()) _o.erase(it);
}

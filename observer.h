#ifndef OBSERVER_H_2YEZMNTW
#define OBSERVER_H_2YEZMNTW

#include <memory>

// declaration
class observee;


/**
 * @brief observer
 */
class observer {
protected:
	std::shared_ptr<observee> _observee;

public:
	observer(const std::shared_ptr<observee>& observee);
	virtual ~observer() = default;
	virtual void update() = 0;
};


#endif /* end of include guard: OBSERVER_H_2YEZMNTW */

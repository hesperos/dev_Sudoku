#ifndef BT_SOLVER_H_ESLD7CET
#define BT_SOLVER_H_ESLD7CET

#include "solver.h"

#include <chrono>
#include <thread>

template <unsigned S>
class bt_solver : public solver<S> {
	constexpr static unsigned _size = S*S;
	unsigned int cnt;
	unsigned int delay;

	bool backtrack_solver() {
		int nx = 0;
		int ny = 0;

		cnt++;

		if (!this->_b->find_empty(nx, ny)) {
			// board is full
			std::cout << "board full" << std::endl;
			return true;
		}

		for (int v = 1; v <= 9; v++) {

			if (this->_b->check_row(ny, v) ||
				this->_b->check_col(nx, v) ||
				this->_b->check_box(nx, ny, v)) {
				continue;
			}

			// move is valid
			solver<S>::_b->set(nx,ny,v);

			if (delay)
				std::this_thread::sleep_for(std::chrono::milliseconds(delay));
			
			if (backtrack_solver()) return true;
			solver<S>::_b->set(nx,ny,0);
		}

		return false;
	}

public:
	bt_solver(std::shared_ptr< board<S> >& b, const int d = 0) :
		solver<S>(b),
		cnt(0),
		delay(d)
	{
	}
	virtual ~bt_solver() = default;

	virtual void solve() {
		cnt = 0;
		bool rv = backtrack_solver();
		std::cout << "(" << rv << ")cnt: " << cnt << std::endl;
	}
};


#endif /* end of include guard: BT_SOLVER_H_ESLD7CET */

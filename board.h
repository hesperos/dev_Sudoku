#ifndef BOARD_H_9OIUK2FG
#define BOARD_H_9OIUK2FG

#include "observee.h"

#include <array>
#include <cstdint>

 /*
  *  Coordinates...
  *
  *     X
  *   +------->
  *   |  
  * Y |   +-------+----
  *   |   |       |
  *   v   |       |
  *       |       |
  *       +-------+----
  *       |       |
  *       |       |
  *       |       |
  */


template<unsigned S>
class board : public observee {
	constexpr static unsigned _size = S*S;
	std::array<int, (_size * _size)> _data;

	inline unsigned _idx(uint8_t x, uint8_t y) const {
		return (x + ( y*_size ));
	}

	bool _find(int& x, int& y, const int& v) const {
		for (int j = 0; j < _size; j++) {
			for (int i = 0; i < _size; i++) {
				if (_data[ _idx(i,j) ] == v) {
					x = i;
					y = j;
					return true;
				} // if
			} // for
		} // for

		return false;
	}

public:
	board() {
		_data.fill(0);
	}

	board(const board<S>& a) {
		_data = a._data;
	}

	void operator=(const board<S>& a) {
		_data = a._data;
	}

	virtual ~board() = default;

	int get(uint8_t x, uint8_t y) const {
		return _data[ _idx(x,y) ];
	}

	void set(uint8_t x, uint8_t y, uint8_t v) {
		int old = _data [ _idx(x,y) ];
		_data [ _idx(x,y) ] = v;

		// update only if the values differ
		if (v != old) update();
	}


	bool check_col(uint8_t x, uint8_t v) const {
		for (int y = 0; y < _size; y++) {
			if (_data[_idx(x, y)] == v) return true;
		}
		return false;
	}

	bool check_row(uint8_t y, uint8_t v) const {
		for (int x = 0; x < _size; x++) {
			if (_data[_idx(x, y)] == v) return true;
		}
		return false;
	}
	
	bool check_box(uint8_t x, uint8_t y, uint8_t v) const {
		int bx = x - (x%S);
		int by = y - (y%S);

		for (int i = 0; i < S; i++) {
			for (int j = 0; j < S; j++) {
				if (_data[_idx(i + bx,j + by)] == v) return true;
			}
		}

		return false;
	}


	bool find_empty(int& x, int& y) {
		return _find(x,y,0);
	}
};


#endif /* end of include guard: BOARD_H_9OIUK2FG */

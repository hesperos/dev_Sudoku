#ifndef CSV_READER_H_8QPSHRU0
#define CSV_READER_H_8QPSHRU0

#include "board_reader.h"
#include <fstream>
#include <sstream>
#include <iterator>

template <unsigned S>
class csv_reader : public board_reader<S> {
public:
	csv_reader(const std::shared_ptr< board<S> >& b) :
		board_reader<S>(b)
	{
	}
	virtual ~csv_reader() = default;

	csv_reader(const csv_reader<S>& a_c) :
		board_reader<S>(a_c)
	{
	}

	virtual void load(const std::string& f) {
		std::ifstream ifs(f);
		if (!ifs.is_open()) {
			throw "Unable to open file: <" + f + ">";
		}

		std::string line;
		int n = 0;

		while ((n < 9) && std::getline(ifs, line)) {
			std::stringstream s(line);
			std::string f;
			int m = 0;

			while (std::getline(s,f,',')) {
				int v = 0;
				std::istringstream(f) >> v;
				board_reader<S>::_b->set(m, n, v);
				m++;
			}
			n++;
		}

		ifs.close();
	}
};

#endif /* end of include guard: CSV_READER_H_8QPSHRU0 */

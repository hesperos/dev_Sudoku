#ifndef SOLVER_H_YVOQCXAP
#define SOLVER_H_YVOQCXAP

#include "board.h"

template <unsigned S>
class solver {
protected:
	std::shared_ptr< board<S> > _b;

public:
	solver(std::shared_ptr< board<S> >& b) :
		_b(b)
	{
	}
	virtual ~solver() = default;

	virtual void solve() = 0;
};


#endif /* end of include guard: SOLVER_H_YVOQCXAP */

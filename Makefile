TARGET=sudoku
SOURCES=\
		observee.cpp \
		observer.cpp \
		main.cpp

DEPS=
COBJ=$(SOURCES:.cpp=.o)

CC=g++
CFLAGS=-std=c++11 -O2
LDFLAGS=

all: $(TARGET)

%.o: %.cpp $(DEPS)
	@echo -e "\tCXX" $<
	@$(CC) -c -o $@ $< $(CFLAGS)

$(TARGET): $(COBJ) 
	@echo -e "\tLINKING CC" $<
	$(CC) -o $(TARGET) $(COBJ) $(LDFLAGS)

clean:
	rm -rf *.o *.hex $(TARGET)	

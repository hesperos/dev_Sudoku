#ifndef OBSERVEE_H_BYWRJCT0
#define OBSERVEE_H_BYWRJCT0

#include "observer.h"

#include <memory>
#include <vector>


/**
 * @brief observee
 */
class observee {
protected:
	std::vector< std::shared_ptr<observer> > _o; // observers

public:
	virtual void update() const;
	virtual void add(const std::shared_ptr<observer>& o);
	virtual void del(const std::shared_ptr<observer>& o);
};


#endif /* end of include guard: OBSERVEE_H_BYWRJCT0 */

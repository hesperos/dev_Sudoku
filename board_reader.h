#ifndef BOARD_READER_H_XT90YCRV
#define BOARD_READER_H_XT90YCRV


template <unsigned S>
class board_reader {
protected:
	std::shared_ptr< board<S> > _b;

public:
	board_reader(const std::shared_ptr< board<S> >& b) :
		_b(b)
	{
	}

	board_reader(const board_reader<S>& a) :
		_b(a._b)
	{
	}

	void operator=(const board_reader<S>& a) {
		_b = a._b;
	}

	virtual ~board_reader() = default;
	virtual void load(const std::string&) = 0;
};



#endif /* end of include guard: BOARD_READER_H_XT90YCRV */

#include <iostream>


#include "board.h"
#include "printer.h"

// solvers
#include "bt_solver.h"

// readers
#include "csv_reader.h"

int main(int argc, char *argv[])
{
	try {
		std::string bf = (argc >=2) ? argv[1] : "board1.csv";
		bool intermediate = false;
		int delay = 0;

		if (argc==1) {
			std::cout << "./sudoku <board_file> [intermediate=0|1] [delay=i]" << std::endl;
			return -1;
		}

		if (argc >= 3) {
			std::istringstream(std::string(argv[2])) >> intermediate;
		}

		if (argc >=4) {
			std::istringstream(std::string(argv[3])) >> delay;
		}

		// board
		auto b = std::make_shared< board<3> >(); 

		// board reader
		auto br = std::make_shared< csv_reader<3> >(b);
		br->load(bf);

		// solver
		auto s = std::make_shared< bt_solver<3> >(b, delay);

		// printer
		auto p = std::make_shared< printer<3> >(b);

		// initialize the printer
		p->init();

		// detach observer & print only the result
		if (!intermediate) b->del(p);

		// solve the puzzle
		s->solve();

		// print the result
		if (!intermediate) p->print();
	}
	catch(std::string& se) {
		std::cout << "[" << se << "]" << std::endl;
	}

	return 0;
}

